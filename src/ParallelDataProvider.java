import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ParallelDataProvider {
	@DataProvider(parallel=true)
	public Object[][] test_data()
	{
		Object[][] obj = new Object[6][2];
		obj[0][0]="Admin";
		obj[0][1]="manager";
		
		obj[1][0] = "Test";
		obj[1][1] = "test124";
		
		obj[2][0] = "prod";
		obj[2][1] = "prod123";
		
		obj[3][0]="Admn";
		obj[3][1]="manager";
		
		obj[4][0] = "Test";
		obj[4][1] = "test124";
		
		obj[5][0] = "prod";
		obj[5][1] = "prod123";
		
		return obj;
	}
	@Test(dataProvider="test_data")
	public void login_app(String username, String pwd)
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\arun\\Documents\\selenium\\ChromeDriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demo.actitime.com/login.do");
		driver.findElement(By.id("username")).sendKeys(username);
		driver.findElement(By.name("pwd")).sendKeys(pwd);
		driver.findElement(By.id("loginButton")).click();
	}
	

}
