package GenericLib;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class DataUtility {
	public String getDatafromProperties(String key) throws IOException
	{
		FileInputStream fis = new FileInputStream("C:\\Users\\arun\\eclipse-workspace\\TestNg\\src\\common.properties");
		Properties pobj = new Properties();
		pobj.load(fis);
		
		return pobj.getProperty(key);
		
	}
	public String getDatafromExcel(String sheetname,int row, int col) throws EncryptedDocumentException, IOException
	{
		FileInputStream fos = new FileInputStream("C:\\Users\\arun\\eclipse-workspace\\TestNg\\src\\Book1.xlsx");
		Workbook book = WorkbookFactory.create(fos);
		Sheet s = book.getSheet(sheetname);
		DataFormatter format = new DataFormatter();
		return format.formatCellValue(s.getRow(row).getCell(col));
	}
}
