package GenericLib;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseClass {
	public WebDriver driver;
	public DataUtility du = new DataUtility();
	
	@BeforeClass
	public void  launch_browser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\arun\\Documents\\selenium\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
	}
	
	@BeforeMethod
	public void login_app() throws IOException
	{
		driver.get(du.getDatafromProperties("url"));
		driver.findElement(By.id("txtUsername")).sendKeys(du.getDatafromProperties("username"));
		driver.findElement(By.id("txtPassword")).sendKeys(du.getDatafromProperties("password"));
		driver.findElement(By.id("btnLogin")).click();
		
	}
	
	@AfterMethod
	public void log_out()
	{
		driver.findElement(By.xpath("//a[@id='welcome']")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();	
	}
	
	@AfterClass
	public void close_browser()
	{
		driver.close();
	}
	
}
