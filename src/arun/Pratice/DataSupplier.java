package arun.Pratice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.DataProvider;

public class DataSupplier {
	@DataProvider(parallel=true)
	public String[][] get_data() throws Exception {
		FileInputStream fis = new FileInputStream("C:\\Users\\arun\\eclipse-workspace\\TestNg\\src\\Book1.xlsx");
		Workbook book = WorkbookFactory.create(fis);
		Sheet s = book.getSheet("Sheet1");
		int a = s.getPhysicalNumberOfRows();
		int b = s.getRow(0).getLastCellNum();
		System.out.println(a);
		System.out.println(b);
		
		String[][] data =new String[a][b];
		for(int i = 0;i<a;i++)
		{
			for(int j=0;j<b;j++)
			{
				DataFormatter df = new DataFormatter();
				data[i][j] = df.formatCellValue(s.getRow(i).getCell(j));
			}
		}
		return data;
		

		
	}

}
