import org.testng.annotations.Test;

public class TestDependency {
	String x = null;
	@Test
	public void CreateShipment()
	{
		System.out.println("Customer Created");
		x="ABC12h";
	}
	@Test
	public void TrackingShipment()
	{
		
		if (x!=null)
		{
			System.out.println("Tracking Shipment");
		}
		else
		{
			System.out.println("Not able to Track");
		}
	}
	@Test
	public void CancelShipment()
	{
		if(x!=null)
		{
			System.out.println("Cancel Shipment");
		}
		else
		{
			System.out.println("Not able to Cancel");
		}
		
		
	}

}
