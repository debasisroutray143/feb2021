package com.hyr.Listener;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;



public class Demo {
	@Test
	public void test_method1()
	{
		System.out.println("Test method 1");
	}
	@Test
	public void test_method2()
	{
		System.out.println("Test method 2");
	}
	@Test(timeOut=1000)
	public void test_method3() throws Exception
	{
		Thread.sleep(2000);
		System.out.println("Test method 3");
	}
	@Test(dependsOnMethods= {"test_method3"})
	public void test_method4()
	{
		System.out.println("Test method 4");
	}
	@Test
	public void test_method5()
	{
		System.out.println("Test method 5");
	}
	@Test
	public void test_method6()
	{
		System.out.println("Test method 6");
	}


}
