package com.hyr.screenshot;


import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class BaseClass {
	public WebDriver driver;
	
	
	@Test(testName="launch_browser")
	public void launch_browser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\arun\\Documents\\selenium\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));		
	}
	
	@Test(testName="searc_google")
	public void searc_google()
	{
		driver.get("https://www.google.com/");
		driver.findElement(By.name("q")).sendKeys("Hyr", Keys.ENTER);
		
		String expexted = "Hyr - Google Search";
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expexted);
	}
	
	@Test(testName="facebook_login")
	public void facebook_login()
	{
		driver.get("https://www.facebook.com/login/");
		driver.findElement(By.id("email")).sendKeys("aru",Keys.ENTER);
		String b = driver.getTitle();
		String c = "Log in to Facebook";
		Assert.assertEquals(b, c);
	
	}
	
	@Test(testName="orange_hrm")
	public void orange_hrm()
	{
		
		driver.get("https://opensource-demo.orangehrmlive.com/");
		
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		
		driver.findElement(By.id("txtPassword")).sendKeys("admin123", Keys.ENTER);
		
		String d = driver.findElement(By.id("welcome")).getText();
		
		System.out.println(d);
		
	}
	
	
	@Test(testName="captureScreenshow")
	public void captureScreenshow(String filename)
	{
		TakesScreenshot ts = (TakesScreenshot) driver;
		File src = ts.getScreenshotAs(OutputType.FILE);
		File dest = new File("./Screenshot/"+filename);
		try
		{
			FileUtils.copyFile(src, dest);
			
			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Sorted");
	}
		
	
	
	@Test(testName="close_browser")
	public void close_browser()
	{
		driver.close();
	}
	
	


}
