import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.DataProvider;

public class ExcelDataSupplier {

	@DataProvider
	public String[][] getData() throws EncryptedDocumentException, IOException {
		// TODO Auto-generated method stub
		FileInputStream fos = new FileInputStream("C:\\Users\\arun\\eclipse-workspace\\TestNg\\src\\Book1.xlsx");
		Workbook book = WorkbookFactory.create(fos);
		Sheet s = book.getSheet("Sheet1");
		//DataFormatter format = new DataFormatter();
		//System.out.println(format.formatCellValue(s.getRow(1).getCell(0)));
		int a = s.getPhysicalNumberOfRows();
		System.out.println(a);
		int b = s.getRow(0).getLastCellNum();
		System.out.println(b);
		
		
		String[][] data = new String[a][b];
		for(int i = 0; i<a; i++)
		{
			for(int j=0; j<b; j++)
			{
				DataFormatter format = new DataFormatter();
				data[i][j]=format.formatCellValue(s.getRow(i).getCell(j));
				
				
			}
			System.out.println();
		}
		return data;
		
		
//		for (Object[] objects : data)
//		{
//			System.out.println(Arrays.toString(objects));
//		}

	}

}
