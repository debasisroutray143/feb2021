import java.io.IOException;
import java.time.Duration;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericLib.DataUtility;

public class DemoTesting {
	WebDriver driver;
	DataUtility du = new DataUtility();
	
	@BeforeClass
	public void  launch_browser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\arun\\Documents\\selenium\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
	}
	
	@BeforeMethod
	public void login_app() throws IOException
	{
		driver.get(du.getDatafromProperties("url"));
		driver.findElement(By.id("txtUsername")).sendKeys(du.getDatafromProperties("username"));
		driver.findElement(By.id("txtPassword")).sendKeys(du.getDatafromProperties("password"));
		driver.findElement(By.id("btnLogin")).click();
		
	}
	
	
	@Test
	public void create_customer() throws EncryptedDocumentException, IOException
	{
		driver.findElement(By.id("menu_pim_viewPimModule")).click();
		driver.findElement(By.id("menu_pim_viewEmployeeList")).click();
		driver.findElement(By.id("empsearch_employee_name_empName")).sendKeys(du.getDatafromExcel("Sheet1",0,0));
		driver.findElement(By.id("empsearch_id")).sendKeys(du.getDatafromExcel("Sheet1",0,1));
		driver.findElement(By.id("empsearch_supervisor_name")).sendKeys(du.getDatafromExcel("Sheet1",0,2));
		//driver.findElement(By.xpath("//input[@id='btnSave']")).click();
		
	}
	
	@AfterMethod
	public void log_out()
	{
		driver.findElement(By.xpath("//a[@id='welcome']")).click();
		driver.findElement(By.xpath("//a[text() = 'Logout']")).click();	
	}
	
	@AfterClass
	public void close_browser()
	{
		driver.close();
	}

}
